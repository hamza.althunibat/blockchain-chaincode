package chaincode

import (
	"encoding/base64"
	"fmt"
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	"github.com/json-iterator/go"
	"log"
	"strings"
)

type ServerConfig struct {
	CCID    string
	Address string
}

// SmartContract provides functions for managing an Certificate
type SmartContract struct {
	contractapi.Contract
}

func (s *SmartContract) IssueCertificate(ctx contractapi.TransactionContextInterface, payload string) error {
	
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	// Verify that the client is submitting request to peer in their organization
	// This is to ensure that a client from another org doesn't attempt to read or
	// write private data from this peer.
	err := verifyClientOrgMatchesPeerOrg(ctx)
	if err != nil {
		return fmt.Errorf("IssueCertificate cannot be performed: Error %v", err)
	}

	// open data model is passed normally

	var openCert OpenCertificate
	err = json.Unmarshal([]byte(payload), &openCert)
	if err != nil {
		return err
	}
	// check if already exist id
	exists, err := s.CertificateExists(ctx, openCert.Id)
	if err != nil {
		return err
	}
	if exists {
		return fmt.Errorf("the asset %s already exists",openCert.Id)
	}

	stub :=ctx.GetStub()
	// validate issuerOrg
	org, err := getSubmittingClientIdentity(ctx)
	if org != openCert.IssuerOrg {
		return fmt.Errorf("the issuer %s doesn't match the payload %s", org, openCert.IssuerOrg)
	}

	// raise the information as event
	err = stub.SetEvent("CertificateIssued",[]byte(payload))
	// store open data
	err = stub.PutState(openCert.Id, []byte(payload))
	if err != nil {
		return err
	}

	// confidential data is passed as private data.

	// Get new certificate from transient map
	transientMap, err := stub.GetTransient()
	if err != nil {
		return fmt.Errorf("error getting transient: %v", err)
	}

	// Asset properties are private, therefore they get passed in transient field, instead of func args
	transientCertJSON, ok := transientMap["cert_data"]
	if !ok {
		//log error to stdout
		return fmt.Errorf("certificate not found in the transient map input")
	}

	var cert Certificate

	err = json.Unmarshal(transientCertJSON, &cert)
	if err != nil {
		return fmt.Errorf("failed to unmarshal JSON: %v", err)
	}
	// validate cert
	if cert.Id != openCert.Id {
		return fmt.Errorf("certificate id %s doesn't match open data certificate id %s", cert.Id, openCert.Id)
	}

	if cert.IssuerOrg != openCert.IssuerOrg {
		return fmt.Errorf("certificate issuer %s doesn't match open data certificate issuer %s", cert.IssuerOrg, openCert.IssuerOrg)
	}
	if strings.TrimSpace(cert.RecipientOrg) == "" {
		return fmt.Errorf("certificate recipient must be provided")
	}
	collection, err := getCollectionName(cert.RecipientOrg, cert.IssuerOrg)

	// Check if asset already exists
	certBytes, err := ctx.GetStub().GetPrivateData(collection, cert.Id)
	if err != nil {
		return fmt.Errorf("failed to get asset: %v", err)
	} else if certBytes != nil {
		fmt.Println("Certificate already exists: " + cert.Id)
		return fmt.Errorf("this certificate already exists: " + cert.Id)
	}

	// Save cert to private data collection
	// Typical logger, logs to stdout/file in the fabric managed docker container, running this chaincode
	// Look for container name like dev-peer0.org1.example.com-{chaincodename_version}-xyz
	log.Printf("CreateAsset Put: collection %v, ID %v, owner %v", collection, cert.Id, org)

	err = ctx.GetStub().PutPrivateData(collection, cert.Id, transientCertJSON)
	if err != nil {
		return fmt.Errorf("failed to put certificate into private data collecton: %v", err)
	}
	return nil
}


// ReadOpenCertificate returns the asset stored in the world state with given id.
func (s *SmartContract) ReadOpenCertificate(ctx contractapi.TransactionContextInterface, id string) (*OpenCertificate, error) {
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	assetJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if assetJSON == nil {
		return nil, fmt.Errorf("the asset %s does not exist", id)
	}

	var openCert OpenCertificate
	err = json.Unmarshal(assetJSON, &openCert)
	if err != nil {
		return nil, err
	}

	return &openCert, nil
}

// ReadCertificate returns the asset stored in the world state with given id.
func (s *SmartContract) ReadCertificate(ctx contractapi.TransactionContextInterface, id string, issuerOrg string) (*Certificate, error) {
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	// Get ID of submitting client identity
	clientID, err := getSubmittingClientIdentity(ctx)
	if err != nil {
		return nil, err
	}
collection, err := getCollectionName(clientID, issuerOrg)
certJSON,err := ctx.GetStub().GetPrivateData(collection,id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from private data %s", err.Error())
	}
	if certJSON == nil {
		return nil, fmt.Errorf("%s does not exist", id)
	}
	var cert Certificate
err = json.Unmarshal(certJSON,&cert)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal private data %s", err.Error())
	}
	return &cert,nil
}


// CertificateExists returns true when asset with given ID exists in world state
func (s *SmartContract) CertificateExists(ctx contractapi.TransactionContextInterface, id string) (bool, error) {
	assetJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return false, fmt.Errorf("failed to read from world state: %v", err)
	}

	return assetJSON != nil, nil
}

// getCollectionName is an internal helper function to get collection of submitting client identity.
func getCollectionName(recipientOrg string, issuerOrg string) (string, error) {
	if strings.TrimSpace(issuerOrg) == "" || strings.TrimSpace(recipientOrg) == "" {
		return "", fmt.Errorf("issuer org or recipient is not provided")
	}
	return issuerOrg + "_" + recipientOrg + "_collection", nil
}

func getSubmittingClientIdentity(ctx contractapi.TransactionContextInterface) (string, error) {
	b64ID, err := ctx.GetClientIdentity().GetID()
	if err != nil {
		return "", fmt.Errorf("failed to read clientID: %v", err)
	}
	decodeID, err := base64.StdEncoding.DecodeString(b64ID)
	if err != nil {
		return "", fmt.Errorf("failed to base64 decode clientID: %v", err)
	}
	return string(decodeID), nil
}

// verifyClientOrgMatchesPeerOrg is an internal function used verify client org id and matches peer org id.
func verifyClientOrgMatchesPeerOrg(ctx contractapi.TransactionContextInterface) error {
	clientMSPID, err := ctx.GetClientIdentity().GetMSPID()
	if err != nil {
		return fmt.Errorf("failed getting the client's MSPID: %v", err)
	}
	peerMSPID, err := shim.GetMSPID()
	if err != nil {
		return fmt.Errorf("failed getting the peer's MSPID: %v", err)
	}

	if clientMSPID != peerMSPID {
		return fmt.Errorf("client from org %v is not authorized to read or write private data from an org %v peer", clientMSPID, peerMSPID)
	}

	return nil
}
